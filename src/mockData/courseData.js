const courseData = [
	{
		id: "wcd001",
		name: "PHP - Laravel",
		description:"Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Officiis, ut quaerat, ex dolore deserunt tempora aliquid tempore, placeat odit consequuntur vitae ea accusamus fugiat accusantium nemo magnam veniam rerum laudantium.",
		price: 45000,
		onOffer: true,

	},
	{
		id: "wcd002",
		name: "Phyton-Django",
		description:"Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Officiis, ut quaerat, ex dolore deserunt tempora aliquid tempore, placeat odit consequuntur vitae ea accusamus fugiat accusantium nemo magnam veniam rerum laudantium.",
		price: 50000,

	},
	{
		id: "wcd003",
		name: "Java-Springboot",
		description:"Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Officiis, ut quaerat, ex dolore deserunt tempora aliquid tempore, placeat odit consequuntur vitae ea accusamus fugiat accusantium nemo magnam veniam rerum laudantium.",
		price: 55000,	
		onOffer: true,

	}


]

export default courseData;