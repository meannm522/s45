import {Card, Button} from 'react-bootstrap'
import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';


export default function CourseCard({courseProp}){
	
	//deconstruct the courseProp into their own variables
	const {name, description, price} = courseProp;

//let's create our useState hooks to store its state
//States are used to keep track of information related to indivisual components

//syntax: const [currentValue (getter), updatedValue(setter)] = useState(InitialGetterValue)

const [count,setCount] = useState(0);
const [seats, setSeats] = useState(30)

//statehook that indicates availability of course for enrollments
const [isOpen, setIsOpen] = useState(true)
console.log(count);

const enroll = () =>{
	
	setCount(count +1)  
	console.log('Enrollees:' + count)
	setSeats(seats-1)
}

useEffect(() => {
	if(seats === 0) {
		setIsOpen(false);
	}
}, [seats]) //it controls the rendering of the useEffect


	return (
			<Card className="m-4">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text> {description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<Card.Text>Enrollees: {count}</Card.Text>
					<Card.Text> Seats: {seats}</Card.Text>
					
					{isOpen ?
						<Button variant="primary" onClick={enroll}> Enroll</Button>
						:
						<Button variant="primary" disable > Enroll</Button>
					}
					
				</Card.Body>	
			</Card>
	

		)
}


//check if the coursecard component is getting the correct proptypes
//PropTypes are used for validating information passed to a to
// component and is a tool normally used to help developers ensure the correct information is passed FROM ONE COMPONENT TO THE NEXT

CourseCard.propTypes = {
	// shape() method is used to check if the prop object conforms to a specific 'shape'
	courseProp:PropTypes.shape({
		//define the properties and their expected types
		name:PropTypes.string.isRequired,
		description:PropTypes.string.isRequired,
		price:PropTypes.number.isRequired
	})

}