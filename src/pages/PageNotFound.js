import React from 'react';
import {Link} from 'react-router-dom';

export default function notFoundPage(){
	return (

		<h1 style = {{TextAlign: "Center"}}> ERROR 404: Page not Found
		<Link to = "/"> Go to home </Link>
		</h1>
		)
}