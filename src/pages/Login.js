import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';


export default function Login() {

	//Allows us to consume the User context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);


	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	
	//login button
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button 
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function authenticate(e) {
		e.preventDefault();

		//Set the email of the authenticated user in the localStorage
		//localStorage.setItem('propertyName', value)
		localStorage.setItem('email', email);

		//Set the global user state to have properties obtained from localStorage
		setUser({
			email: localStorage.getItem('email')
		})



		Swal.fire({
			title: "Yay!",
			icon: 'success',
			text: `${email} Successfully Logged in`
		})

		setEmail('');
		setPassword('');


	}


	return (

		// Create a conditional rendering statement that will redirect the user to the courses page when a user is logged in.

		(user.email !== null) ?

		<Navigate to="/courses" />

		:

		<Form onSubmit={(e) => authenticate(e)}>
			<h1>Login</h1>
			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					required
					value={email}
					onChange={(e) => setEmail(e.target.value)}

				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter your password"
					required
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					
				/>
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" className="mt-3"> Login </Button>

				:

				<Button variant="danger" type="submit" disabled className="mt-3"> Login </Button>
			}

			
		</Form>
		)
}

